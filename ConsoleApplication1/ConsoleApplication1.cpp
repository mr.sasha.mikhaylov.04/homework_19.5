#include <iostream>

class Animal {
public:
    Animal() {

    }

    virtual void Voice() {
        std::cout << "Voice\n";
    }
};

class Dog : public Animal {
public:
    Dog() {

    }

    void Voice() override {
        std::cout << "Woof\n";
    }
};

class Cat : public Animal {
public:
    Cat() {

    }

    void Voice() override {
        std::cout << "Meow\n";
    }
};

class Cow : public Animal {
public:
    Cow() {

    }

    void Voice() override {
        std::cout << "Moo\n";
    }
};

int main()
{
    Cat cat;
    Dog dog;
    Cow cow;

    Animal* animals[3]{&cat, &dog, &cow};

    for (int i = 0; i < 3; i++)
    {
        animals[i]->Voice();
    }
}